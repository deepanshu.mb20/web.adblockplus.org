TEST_DATA = [(
      'desktop_chrome',
      'Get Adblock Plus for Chrome',
      'https://eyeo.to/adblockplus/chrome_install/chrome',
      'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 '
      '(KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36'
), (
      'desktop_firefox',
      'Agree and Install for Firefox',
      'https://eyeo.to/adblockplus/firefox_install/firefox',
      'Mozilla/5.0 (Windows NT 5.1; rv:7.0.1) Gecko/20100101 Firefox/7.0.1'
), (
      'desktop_internet_explorer',
      'Agree and Install for Internet Explorer',
      'https://eyeo.to/adblockplus/ie_install/internet-explorer',
      'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.1.4322)'
), (
      'desktop_safari',
      'Agree and Install for Safari',
      'https://eyeo.to/adblockplus/safari_install/safari',
      'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_6; en-en) AppleWebKit/533.19.4 '
      '(KHTML, like Gecko) Version/5.0.3 Safari/533.19.4'
), (
      'desktop_edge',
      'Get Adblock Plus for Microsoft Edge',
      'https://eyeo.to/adblockplus/edge_install/edge',
      'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
      'Chrome/51.0.2704.79 Safari/537.36 Edge/14.14393'
), (
      'desktop_opera',
      'Get Adblock Plus for Opera',
      'https://eyeo.to/adblockplus/opera_install/opera',
      'Opera/9.80 (Windows NT 6.1; WOW64) Presto/2.12.388 Version/12.18'
), (
      'desktop_yandex',
      'Get Adblock Plus for Yandex Browser',
      'https://eyeo.to/adblockplus/yandex_install/yandex-browser',
      'Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 '
      'YaBrowser/17.6.1.749 Yowser/2.5 Safari/537.36'
), (
      'mobile_samsung_internet_android',
      'Agree and Install for Samsung Internet',
      'https://eyeo.to/adblockplus/android_samsung_install/android',
      'Mozilla/5.0 (Linux; Android 7.0; SAMSUNG SM-G610M Build/NRD90M) AppleWebKit/537.36 '
      '(KHTML, like Gecko) SamsungBrowser/7.4 Chrome/59.0.3071.125 Mobile Safari/537.36'
)]
